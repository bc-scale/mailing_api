from rest_framework import serializers
from mailing.models import Customer, Message, Mailing


class CustomerSerializer(serializers.ModelSerializer):
    """Сериализатор сущностей типа Customer"""

    class Meta:
        fields = '__all__'
        model = Customer

    def phone_number_validation(self, value):
        """Функция валидации номера телефона"""

        if str(value)[0] != '7':
            raise serializers.ValidationError(
                'Неверный формат номера телефона!'
            )
        return value


class MailingsSerializer(serializers.ModelSerializer):
    sent_messages = serializers.SerializerMethodField()
    not_sent_messages = serializers.SerializerMethodField()

    class Meta:
        fields = (
            'id', 'start_time', 'end_time',
            'msg_text', 'tag', 'operator_code', 'sent_messages', 'not_sent_messages'
        )
        model = Mailing

    """Функция возвращает количество успешно отправленных сообщений"""
    def get_sent_messages(self, objct):
        return objct.messages.filter(status='S').count()

    """Функция возвращает количество неудачно отправленных сообщений"""
    def get_not_sent_messages(self, objct):
        return objct.messages.filter(status='NS').count()


class MessageSerializer(serializers.ModelSerializer):
    """Сериализатор сущностей типа Message"""

    class Meta:
        fields = (
            'id', 'sending_time', 'state', 'mailing', 'customer'
        )
        model = Message


class MailingSerializer(serializers.ModelSerializer):
    """Сериализатор сущностей типа Mailing"""

    messages = MessageSerializer(read_only=True, many=True)

    class Meta:
        fields = (
            'id', 'start_time', 'end_time',
            'msg_text', 'tag', 'operator_code', 'messages'
        )
        model = Mailing
