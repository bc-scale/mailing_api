from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

from mailing.models import Customer, Message, Mailing

from .serializers import CustomerSerializer, MailingSerializer, MailingsSerializer, MessageSerializer

# Create your views here.


class CustomerViewSet(viewsets.ModelViewSet):
    """
    Viewset для просмотра и редактирования экземпляров класса Customer
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('tag', 'operator_code')


class MailingViewSet(viewsets.ModelViewSet):
    """
    Viewset для просмотра и редактирования экземпляров класса Mailing
    """
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return MailingsSerializer
        return MailingSerializer


class MessageViewSet(viewsets.ModelViewSet):
    """
    Viewset для просмотра и редактирования экземпляров класса Message
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
