from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CustomerViewSet, MessageViewSet, MailingViewSet

app_name = 'api'

router_v1 = DefaultRouter()
router_v1.register('customers', CustomerViewSet)
router_v1.register('mailings', MailingViewSet)
router_v1.register('messages', MessageViewSet)

urlpatterns = [
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),
    path('', include(router_v1.urls))
]