#  Сервис уведомлений



## Техническое задание

[https://www.craft.do/s/n6OVYFVUpq0o6L](https://www.craft.do/s/n6OVYFVUpq0o6L)

## Описание проекта

- Реализация тестового задания на вакансию Python Developer
- В основе проекта лежит **DRF** (djangorestframework)
- API сервис для создания и управления рассылками, взаимодействия данными клиентов

## Стек технологий

- Python 3.8
- Django 3.1.14
- DRF
- PostgreSQL
- Celery
- Redis
- Docker
- Flower

## Установка и запуск проекта

1. Склонировать репозиторий с GitLab:

````
git clone git@gitlab.com:basbky/mailing_api.git
````
2. Перейти в директорию проекта

3. Создать виртуальное окружение:

````
python -m venv venv
````

4. Активировать окружение:

````
source venv/bin/activate
````

5. Заполнить переменные окружения в файле .env:

````
URL=<your_url>
SECRET_KEY=<django_secret_key>
API_SENDING_TOKEN=<api_token>
````

6. Установка зависимостей в виртуальное окружение:

````
pip install -r requirements.txt
````

7. Создать и инициализировать миграции в базу данных:
````
python manage.py makemigrations
python manage.py migrate
````

8. Запустить сервер
````
python manage.py runserver
````

9. Запустить celery
````
celery -A mailing_api worker -l info
````

10. Запустить flower
````
celery -A mailing_api flower --port=5555
````

## Установка с помощью docker-compose:

1. Склонировать репозиторий с GitLab:

````
git clone git@gitlab.com:basbky/mailing_api.git
````
2. Перейти в директорию проекта

3. Заполнить переменные окружения в файле .env:

````
URL=<your_url>
SECRET_KEY=<django_secret_key>
API_SENT_TOKEN=<api_token>
````

4. Запустить контейнеры 
```` 
docker-compose up -d
````

5. Остановка работы контейнеров 
````
docker-compose stop
````
***
# Основные ссылки

`http://0.0.0.0:8000/api/` - API проекта

`http://0.0.0.0:8000/api/customers/` - Клиенты

`http://0.0.0.0:8000/api/messages/` - Сообщения

`http://0.0.0.0:8000/api/mailings/` - Рассылки

`http://0.0.0.0:8000/docs/` - Документация проекта

`http://0.0.0.0:5555` - Мониторинг задач Flower
***

# Выполненные дополнительные задания

1. **подготовить docker-compose для запуска всех сервисов проекта одной командой**

2. **сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io**

3. **удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.**
***
