from django.db import models
import pytz
# Create your models here.


class Customer(models.Model):
    """Модель сущности Customer"""
    TIMEZONE_LIST = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phone_number = models.CharField(verbose_name='Номер телефона', max_length=15)
    operator_code = models.CharField(verbose_name='Код мобильного оператора', max_length=4, editable=False)
    tag = models.CharField(verbose_name='Теги', max_length=100)
    time_zone = models.CharField(verbose_name='Часовой пояс', max_length=32,
                                 choices=TIMEZONE_LIST, default='UTC')

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Mailing(models.Model):
    """Модель сущности Mailing"""

    msg_text = models.TextField(verbose_name='Текст сообщения', max_length=255)
    operator_code = models.CharField(verbose_name='Код мобильного оператора', max_length=4, blank=True)
    tag = models.CharField(verbose_name='Теги', max_length=60)
    start_time = models.DateTimeField(verbose_name='Время начала рассылки')
    end_time = models.DateTimeField(verbose_name='Время окончания рассылки')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Message(models.Model):
    """Модель сущности Message"""

    STATUS_CHOICES = (
        ('S', 'SENT'),
        ('NS', 'NOT SENT'),
    )

    state = models.CharField(max_length=2, choices=STATUS_CHOICES, verbose_name='Статус доставки сообщения')
    sending_time = models.DateTimeField(auto_now_add=True, verbose_name='Время доставки сообщения')

    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Клиент'
    )

    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Рассылка'
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
