
class MissingEnvError(Exception):
    """Исключение при недостающих переменных окружения"""


class APIException(Exception):
    """Исключение при ошибке запроса к API"""