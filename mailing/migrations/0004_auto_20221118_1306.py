# Generated by Django 3.1.14 on 2022-11-18 10:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mailing', '0003_auto_20221118_1303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone_number',
            field=models.CharField(max_length=15, verbose_name='Номер телефона'),
        ),
        migrations.AlterField(
            model_name='message',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailing.customer', verbose_name='Клиент'),
        ),
        migrations.AlterField(
            model_name='message',
            name='mailing',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailing.mailing', verbose_name='Рассылка'),
        ),
        migrations.AlterField(
            model_name='message',
            name='sending_time',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Время доставки сообщения'),
        ),
        migrations.AlterField(
            model_name='message',
            name='state',
            field=models.CharField(choices=[('S', 'SENT'), ('NS', 'NOT SENT')], max_length=2, verbose_name='Статус доставки сообщения'),
        ),
    ]
