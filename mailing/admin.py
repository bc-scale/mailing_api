from django.contrib import admin
from .models import Customer, Mailing, Message
# Register your models here.

admin.site.register(Customer)
admin.site.register(Mailing)
admin.site.register(Message)