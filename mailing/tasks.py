import os
import sys
import time
import logging
from datetime import datetime

from mailing_api.celery import app

import requests
from dotenv import load_dotenv

from mailing.exceptions import MissingEnvError

from mailing.models import Customer, Message, Mailing

load_dotenv()

"""Конфигурация логирования"""
logging.basicConfig(
    level=logging.ERROR,
    handlers=[logging.StreamHandler(sys.stdout)],
    format='%(asctime)s %(name)s %(levelname)s  %(message)s',
)
logger_dispatcher = logging.getLogger(__name__)

logger_dispatcher.setLevel(logging.DEBUG)

TIME_FORMAT = "%Y-%m-%d - %H:%M:%S"

RETRY_TIME = 15

API_SENT_TOKEN = os.getenv('API_SENDING_TOKEN')


def api_send_message(msg_id, customer, message):
    """Функция отправки сообщения через внешнее API"""

    headers = {'Authorization': f'{API_SENT_TOKEN}'}
    json = {
        'phone_number': customer,
        'text': message
    }

    try:
        response = requests.post(
            f'{os.getenv("URL", default="https://probe.fbrq.cloud/v1/send/")}{msg_id}',
            headers=headers,
            json=json
        )
        logger_dispatcher.info('Сообщение было отправлено!')
        return True if response.status_code == 200 else False

    except requests.exceptions.RequestException as error:
        logger_dispatcher.error(f'Произошёл сбой при отправке сообщения {error}!')
        return False


@app.task()
def start_mailing():
    """Функция обработки рассылок"""
    logger_dispatcher.debug('Старт!')
    msg_id = [1]
    end_mailing_id = []

    if API_SENT_TOKEN is None:
        logger_dispatcher.critical('Не хватает переменных окружения!')
        raise MissingEnvError()

    while True:
        try:
            logger_dispatcher.debug('Начало попытки')
            mailings = Mailing.objects.all()
            for mailing in mailings:
                datetime_now = datetime.now()
                if datetime.strptime(mailing['start_time'], TIME_FORMAT) \
                        <= datetime_now <= datetime.strptime(mailing['end_time'], TIME_FORMAT) \
                        and mailing['id'] not in end_mailing_id:

                    mailing_id = mailing['id']
                    operator_code = mailing['operator_code']
                    text = mailing['text']
                    tag = mailing['tag']

                    customers = Customer.objects.filter(tag=tag).filter(operator_code=operator_code)

                    for customer in customers:
                        datetime_now = datetime.now()
                        customer_id = customer['id']
                        if (
                            datetime_now <= datetime.strptime(
                                mailing['end_time'], TIME_FORMAT
                            ) and api_send_message(
                                msg_id[0],
                                customer,
                                text
                            )
                        ):
                            Message.objects.create(
                                status='S',
                                customer=customer_id,
                                mailing=mailing_id
                            )
                        else:
                            Message.objects.create(
                                status='NS',
                                customer=customer_id,
                                mailing=mailing_id
                            )
                        msg_id[0] += 1
                    end_mailing_id.append(mailing_id)
            logger_dispatcher.debug('Конец попытки')
            time.sleep(RETRY_TIME)
        except requests.exceptions.RequestException as error:
            logger_dispatcher.error(f'Ошибка в работе функции: {error}')
            time.sleep(RETRY_TIME)
